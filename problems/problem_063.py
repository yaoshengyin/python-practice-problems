# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    result = ''
    for char in word:
        if char == 'z':
            result += 'a'
        elif char == 'Z':
            result += 'A'
        else:
            result += chr(ord(char) + 1)
    return result

# Test examples
print(shift_letters("import")) # Output: "jnqpsu"
print(shift_letters("ABBA"))   # Output: "BCCB"
print(shift_letters("Kala"))   # Output: "Lbmb"
print(shift_letters("zap"))    # Output: "abq"
